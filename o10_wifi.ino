#include <ESP8266WiFi.h>

WiFiClient espClient;

void wifi_setup(String wifiSsid, String wifiPwd) {
  Serial.print("Wifi setup - start");

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Wifi setup - Configuring for SSID: "); Serial.println(wifiSsid.c_str());
  WiFi.begin(wifiSsid.c_str(), wifiPwd.c_str());

  int linelength = 0;
  Serial.print("Wifi setup - connecting:");
  while (WiFi.status() != WL_CONNECTED) {
    builtInLedOn();
    delay(200);
    builtInLedOff();
    delay(500);
    Serial.print(".");
    linelength++;
    if (linelength++ > 80) {
      Serial.println("");
      Serial.print("Wifi setup - still connecting:");
      linelength = 0;
    }
  }
  Serial.println("");

  Serial.println("Wifi setup - WiFi connected");
  Serial.println("Wifi setup - IP address: ");
  Serial.println(WiFi.localIP());
  builtInLedOn();

  Serial.println("Wifi setup - end");
}
