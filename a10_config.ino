#include <WString.h>
#include <ArduinoJson.h>
#include "FS.h"

using namespace std;

class ConfigClass {
  private:
    String _configFileName = "/config.json";
    const String _configInitialisedKey = "configInitialised";
    void(*_initialiseCallback)(JsonObject&) = 0;

  public:
    // The Configurable properties
    String wifiExternalSSID = "config_wifi_ext_ssid";
    String wifiExternalPwd = "config_wifi_ext_pwd";
    String mqttServer = "mqtt_server";
    String mqttId = "mqtt_id";
    String rebootsKey = "reboots";
    int reboots = -1;

  private:

    void innerLoad(JsonObject& json) {
      Serial.println("inner loading json config...");
      json.printTo(Serial);
      Serial.println("");
      if (json.containsKey("wifiExternalSSID")) {
          Serial.println("Config: EXISTS");
      } else {
          Serial.println("Config: NOT EXISTS");
      }
      wifiExternalSSID = (const char*)json["wifiExternalSSID"];
      wifiExternalPwd = (const char*)json["wifiExternalPwd"];
      mqttServer = (const char*)json["mqttServer"];
      mqttId = (const char*)json["mqttId"];
      reboots = json["reboots"];
    }

    void innerSave(JsonObject& json) {
      json["wifiExternalSSID"] = wifiExternalSSID.c_str();
      json["wifiExternalPwd"] = wifiExternalPwd.c_str();
      json["mqttServer"] = mqttServer.c_str();
      json["mqttId"] = mqttId.c_str();
      json["reboots"] = reboots;
    }

    void innerLog() {
      Serial.print("Config: wifiExternalSSID: ");   Serial.println(wifiExternalSSID.c_str());
      Serial.print("Config: wifiExternalPwd: ");   Serial.println(wifiExternalPwd.c_str());
      Serial.print("Config: mqttServer: ");   Serial.println(mqttServer.c_str());
      Serial.print("Config: mqttId: ");   Serial.println(mqttId.c_str());
      Serial.print("Config: reboots: ");      Serial.println(reboots);
    }

  public:
    bool spiffs_begin() {
      if (!SPIFFS.begin()) {
        Serial.println("Config: Failed to mount file system");
        return false;
      }
      return true;
    }

    bool load() {
      Serial.println("Config: Directory of /");
      Dir dir = SPIFFS.openDir("/");
      while (dir.next()) {
          Serial.print(dir.fileName());
          File f = dir.openFile("r");
          Serial.println(f.size());
      }

      File configFile = SPIFFS.open(_configFileName.c_str(), "r");
      if (!configFile) {
        Serial.print("Config: Failed to open config file ");
        Serial.println(_configFileName.c_str());
        return false;
      }
      Serial.print("Config: Opened ");
      Serial.println(_configFileName.c_str());

      size_t size = configFile.size();
      if (size > 1024) {
        Serial.println("Config: Config file size is too large");
        return false;
      }
      string logmsg = "Config: Config file size = ";
      logmsg += (unsigned int)(size);
      Serial.println(logmsg.c_str());

      std::unique_ptr<char[]> buf(new char[size]);
      configFile.readBytes(buf.get(), size);
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json = jsonBuffer.parseObject(buf.get());

      if (!json.success()) {
        Serial.print("Config: Failed to parse config file ");
        Serial.println(_configFileName.c_str());
        return false;
      }
      Serial.println("Config: Json parsed successfully ");

      if (configNotInitialised(json)) {
          initialiseConfig(json);
      }
      innerLoad(json);
      Serial.println("Config: Inner load complete");
      log();

      return true;
    }

    bool configNotInitialised(JsonObject& json) {
        if (json.containsKey(_configInitialisedKey.c_str())) {
            return false;
        } else {
            return true;
        }
    }

    void setInitialiseCallback(void(*initialiseCallback)(JsonObject& json)) {
        _initialiseCallback = initialiseCallback;
    }

    void initialiseConfig(JsonObject& json) {
        if (_initialiseCallback != 0) {
            _initialiseCallback(json);
        }
    }

    bool save() {
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json = jsonBuffer.createObject();
      innerSave(json);
      json[_configInitialisedKey.c_str()] = true;

      File configFile = SPIFFS.open(_configFileName.c_str(), "w");
      if (!configFile) {
        Serial.print("Config: Failed to open config for writing the file ");
        Serial.println(_configFileName.c_str());
        return false;
      }

      json.printTo(configFile);
      return true;
    }

    void log() {
        Serial.println("Config:");
        innerLog();
    }
};

ConfigClass Config;
