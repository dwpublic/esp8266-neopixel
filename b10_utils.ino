#include <vector>
#include <WString.h>

using namespace std;

vector<String> strsplit(const char* str, char c = ' ')
{
    vector<String> result;
    String s = String(str);

    int lastStart = 0;
    int current;
    for (current = 0; current < s.length(); current++) {
        if (s.charAt(current) == c) {
            result.push_back(s.substring(lastStart, current));
            current++;
            lastStart = current;
        }
    }
    if (lastStart != current) {
        result.push_back(s.substring(lastStart));
    }
    // do
    // {
    //     const char *begin = str;
    //
    //     while(*str != c && *str) {
    //         str++;
    //     }
    //     if (*str == c) {
    //         *str = 0;
    //     }
    //     result.push_back(String(begin));
    // } while (0 != *str++);

    return result;
}

vector<String> strsplit(String stdstr, char c = ' ')
{
  return strsplit(stdstr.c_str(), c);
}
