#include <Ticker.h>

int _blinkerPin;
int _blinkerPeriod;
Ticker _blinkerTicker;
bool _blinkerState = -1;

void BlinkerTick() {
  //toggle state
  _blinkerState = ! _blinkerState;
  digitalWrite(_blinkerPin, _blinkerState);
}

void BlinkerSetup(int pin) {
  pinMode(pin, OUTPUT);
  _blinkerPin = pin;
  _blinkerPeriod = -1;
  _blinkerState = false;
}

void BlinkerSetPeriod(int period) {
//   if (_blinkerPeriod != -1) {
//     _blinkerTicker.detach();
//   }
//   _blinkerPeriod = period;
//   _blinkerTicker.attach_ms(_blinkerPeriod, BlinkerTick);
}
